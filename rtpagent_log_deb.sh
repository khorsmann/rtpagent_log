#!/bin/bash

###
### (C) QXIP B.V. 2020-2021 (http://www.qxip.net)
### Author: Michele Campus (mcampus@qxip.net)
###

### for Debian ###

echo "  _ \ __ __|  _ \      \                         |"
echo " |   |   |   |   |    _ \     _  |   _ \  __ \   __|"
echo " __ <    |   ___/    ___ \   (   |   __/  |   |  |"
echo "_| \_\  _|  _|     _/    _\ \__. | \___| _|  _| \__|"
echo "                            |___/"

### RTPAgent Logs
FILE_R=rtpagent_log.txt
cat /var/log/syslog | grep rtpagent >> "$FILE_R"

### RTPAgent version
/usr/local/rtpagent/bin/rtpagent -v > "version.txt"

### ZIP or TAR file or rtpagent config
CONFIG_PATH=/usr/local/rtpagent/etc
z=`which -a zip`
if [ -n "$z" ]; then
        ZIP_FILE=rtpagent_config.zip
        zip -r "$ZIP_FILE" "$CONFIG_PATH"
else
	TAR_FILE=rtpagent_config.tar.gz
	tar -czf "$TAR_FILE" -P "$CONFIG_PATH"
fi

### System Logs
FILE_S=system_info.txt
date >> "$FILE_S"
echo -e "\n" >> "$FILE_S"

# check the existance of commands needed for system monitoring
if ! command -v iostat &> /dev/null
then
    apt-get -y install sysstat
fi
if ! command -v vmstat &> /dev/null
then
    apt-get -y install sysstat
fi
if ! command -v netstat &> /dev/null
then
    apt-get -y install net-tools
fi

echo "##### IOSTAT #####" >> "$FILE_S"
iostat -p >> "$FILE_S"
echo -e "\n" >> "$FILE_S"
echo "##### VMSTAT #####" >> "$FILE_S"
vmstat -t >> "$FILE_S"
echo -e "\n" >> "$FILE_S"
echo "##### NETSTAT #####" >> "$FILE_S"
netstat -anus >> "$FILE_S"
echo -e "\n" >> "$FILE_S"
echo "##### LSOF for TCP #####" >> "$FILE_S"
lsof -ni:9062 -sTCP:ESTABLISHED | wc -l >> "$FILE_S"
lsof -ni:9062 -sTCP:ESTABLISHED >> "$FILE_S"
echo -e "\n" >> "$FILE_S"
echo "#### Memory info stack and heap (ulimit) ####" >> "$FILE_S"
ulimit -a >> "$FILE_S"
echo -e "\n" >> "$FILE_S"

### END: Merge all into one single compressed file
now=$(date +"%d_%m_%Y")
hostname=`hostname`
if [ -n "$z" ]; then
	ZIP_FINAL=rtpagent_"$hostname"_"$now".zip
	zip "$ZIP_FINAL" "$ZIP_FILE" "$FILE_S" "$FILE_R" version.txt
	rm -rf "$ZIP_FILE" "$FILE_S" "$FILE_R" version.txt
else
	TAR_FINAL=rtpagent_trbl_"$now".tar.gz
	tar -czf "$TAR_FINAL" -P "$TAR_FILE" "$FILE_S" "$FILE_R" version.txt
	rm -rf "$TAR_FILE" "$FILE_S" "$FILE_R" version.txt
fi
