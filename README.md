# RTPAgent_LOG

RTPAgent log scripts for Debian and Centos

### DEBIAN

run `rtpagent_log_deb.sh`

### CENTOS 7

run `rtpagent_log_el7.sh`


The scripts generate a unique zip (or tar) file with all the RTPAgent information for troubleshooting

NOTE: consider to put your debug level to 10 in `/usr/local/rtpagent/etc/rtpagent.xml` and restart the rtpagent service in order to generate log informations before running the scripts.
